import { Component } from '@angular/core';
import { double } from '@dazraf/my-lib';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = '@dazraf/ui';
  number = 42;
  doubled = double(this.number);
}
